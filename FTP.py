from q_item import Packet, Event 
import numpy as np 
import heapq
import math
import random
from jacobson import Jacobson 
from TCP import TCP
import sys

class FTP(TCP): 

    W = 1
    time = 0.0 
    count = 1

    #constructor set to default Jacobson-- parameter other TCP 
    def __init__(self, p = Jacobson(), num_times = 15):
        TCP.__init__(self)
        self.num_times = 0
        self.counter = 0


    def start_program(self):
        self.add_to_q(Packet.TCP_REQ)
        self.send_packets()

    def prep_object(self):
        S = 0
        while S < 50 or S > 1048576 * 2:
            S = np.random.lognormal(14.45, 0.35)

        MTU = 0
        selection = random.random()
        if selection < 0.76:
            MTU = 1500
        else:
            MTU = 576

        return (S, MTU)

    #sends an object to the other TCP by calling the send_packets function
    def send_object(self): 
        (S, MTU) = self.prep_object()
        TCP.send_object(self, S, MTU)

    #divides the objects into 40 byte packets 
    def read_object(self): 
        Dpc = np.random.exponential(180)
        TCP.time += Dpc

     #if packet is received append rtt to rtt history list 
    def receive_packets(self, packets):
        for packet in packets:
            if packet.state == Packet.TCP_REQ: 
                self.send_object()
                self.send_packets()
        if any(packet.state == Packet.TCP_RESP for packet in packets):
            self.read_object()
            #if self.P > 0: 
                #while self.P > 0: 
                    #TCP.add_to_q(Packet.TCP_REQ)
                #self.send_packets()
            if self.counter < self.num_times:
                self.counter += 1
                self.start_program()
            else:
                print type(self.p).__name__, self.p.lost_packets*100.0/TCP.count
                sys.exit()




                
            