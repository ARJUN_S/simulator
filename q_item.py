class QItem:
	#constructor takes state and time parameter
	def __init__(self, state, time):
		self.state = state
		self.t = time

	def __cmp__(self, other): 
		return cmp(self.t, other.t)


class Event(QItem): 
	#events for connection and expiration
	EXP, CONNECT = range(1, 3)

	def __init__(self, state, time, packet):
		QItem.__init__(self, state, time)
		self.packet = packet

	def __repr__(self):
		return 'E[' + str(self.state) + ', ' + str(self.t) + ', ' + str(self.packet) + ']'

class Packet(QItem):
	#states of the packet
	TCP_SYN, TCP_SYNACK, TCP_ACK, TCP_REQ, TCP_RESP, TCP_FIN = range(1, 7)
	#also contains an rtt
	def __init__(self, state, tc, time):
		QItem.__init__(self, state, time)
		self.tc = 0

	def __repr__(self):
		return 'P[' + str(self.state) + ', ' + str(self.t) + ', ' + str(self.tc) + ']'

	def __eq__(self, other):
		if self.state == other.state and self.t == other.t and self.tc == other.tc:
			return True
		return False
	