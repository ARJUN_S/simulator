from q_item import Packet, Event 
import numpy as np 
import heapq
import math
from jacobson import Jacobson 
from TCP import TCP

class HTTPPer(TCP): 

    #constructor set to default Jacobson-- parameter other TCP 
    def __init__(self, p = Jacobson()):
        TCP.__init__(self)
        self.counter = 0
        self.Nd = 54 
        while self.Nd <= 0 or self.Nd > 53: 
            self.Nd = int(np.random.pareto(1.1) * 55 - 2)

    def start_program(self):
        heapq.heappush(self.q, Event(Event.CONNECT, 0, None))
        self.remove_from_q()
        self.add_to_q(Packet.TCP_SYN)
        #self.test.test_start_program(self.q) 
        self.send_packets()

    def prep_object(self, flag):
        S = 0
        if flag:
            while S < 50 or S > 1048576 * 2:
                S = np.random.lognormal(8.35, 1.37)
        else:
            while S < 50 or S > 1048576 * 2:
                S = np.random.lognormal(6.17, 2.36)
        #self.test.test_prep_object(S)
        return (S, 1480)

    #sends an object to the other TCP by calling the send_packets function
    def send_object(self, flag): 
        (S, MTU) = self.prep_object(flag)
        #self.test.test_send_object(MTU) 
        TCP.send_object(self, S, MTU)

    #divides the objects into 40 byte packets 
    def parse_object(self): 
        x = TCP.time
        Tp = np.random.exponential(0.13)
        TCP.time += Tp
        #self.test.test_parse_object(x, Tp, TCP.time)

    #if packet is received append rtt to rtt history list 
    def receive_packets(self, packets): 
        for packet in packets:
            if packet.state == Packet.TCP_SYN: 
                self.add_to_q(Packet.TCP_SYNACK)
            elif packet.state == Packet.TCP_SYNACK: 
                self.add_to_q(Packet.TCP_ACK)
            elif packet.state == Packet.TCP_ACK: 
                self.OTCP.control.add_to_q(Packet.TCP_REQ)
                self.OTCP.control.send_packets()
            elif packet.state == Packet.TCP_REQ:
                self.send_object(True)
            elif packet.state == Packet.TCP_RESP:
                self.parse_object()
                if self.counter < self.Nd: 
                    for x in range(0, 4): 
                        self.send_object(False)
                        self.counter += 1
                else:
                    self.add_to_q(Packet.TCP_FIN)
        #self.test.test_receive_packets()
        self.send_packets()

    def print_loss(self):
        #self.test.test_print_loss()
        print type(self.p).__name__, self.p.lost_packets*100.0/TCP.count

