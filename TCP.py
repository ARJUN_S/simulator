from q_item import Packet, Event 
import numpy as np 
import heapq
import math
import sys
from jacobson import Jacobson 
from TCPtest import TestTCP

class TCP:

    W = 1
    time = 0.0 
    count = 1
    tC = 0 

    '''events = 0
    packs = 0'''

    def __init__(self, p = Jacobson()):
        self.p = p 
        self.q = []
        self.OTCP = None
        self.packets = []
        self.P = 0 

    #sends an object to the other TCP by calling the send_packets function
    def send_object(self, S, MTU): 

        N = int(math.ceil(S / (MTU - 40)))

        C = int(min(N, TCP.W))
        P = N - C

        for x in range(0, N): 
            if x < C: 
                tc = self.get_tc()
                self.add_to_q(Packet.TCP_RESP)
            else: 
                self.wait_tc()
                self.add_to_q(Packet.TCP_RESP)

        x = TCP.W
        PW = N + TCP.W
        TCP.W = PW
        #self.test.test_send_object(N, C, P, x, PW)

    #sends a packet to the other TCP 
    def send_packets(self):
        curr_packs = []
        while len(self.q) > 0:
            element = self.remove_from_q() 
            if isinstance(element, Event) and element.packet in self.packets:
                continue
            elif isinstance(element, Event) and element.packet not in self.packets:
                self.p.process_loss(element.packet.tc, TCP.time)
                heapq.heappush(self.q, Event(Event.EXP, self.p.rto * 2 + TCP.time, element.packet))
                TCP.W /= 2
            else:
                if element.state == Packet.TCP_FIN: 
                    self.print_loss()
                    sys.exit()
                self.packets.append(element)
                curr_packs.append(element)
            TCP.count += 1
        self.OTCP.control.receive_packets(curr_packs)

    def wait_tc(self):
       TCP.tC = np.random.exponential(50)

    #returns the calculated tc variable
    def get_tc(self): 
        x = TCP.time
        tc = np.random.exponential(50) 
        TCP.time += tc
        #self.test.test_get_tc(x, tc, TCP.time)
        return tc

    #sets the other TCP 
    def set_other(self, OTCP):
        self.OTCP = OTCP

    #adds the given packet to the queue
    def add_to_q(self, state): 
        x = len(self.q)
        tc = self.get_tc()
        packet = Packet(state, tc, TCP.time)
        self.p.receive_rtt(tc, TCP.time)
        while tc > self.p.rto:
            self.p.rto *= 2
        heapq.heappush(self.q, packet)
        heapq.heappush(self.q, Event(Event.EXP, TCP.time - tc + self.p.rto, packet))
        print tc, TCP.time, self.p.rto
        #self.test.test_add_to_q(x, len(self.q))

    #removes and returns the lowest item in the heap
    def remove_from_q(self): 
        x = len(self.q)
        val = heapq.heappop(self.q) 
        #self.test.test_remove_from_q(x, len(self.q))
        return val
