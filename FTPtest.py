import unittest
from q_item import Packet
from FTP import FTP
from TCP import TCP
from client_server import Client, Server 

class TestHTTPPer(unittest.TestCase):

	def setUp(self):
		self.c = Client()
		self.s = Server()
		self.c.configure(self.s)
		self.s.configure(self.c)

	def test_start_program(self):
		try:
			self.c.control.start_program()
			self.assertTrue(len(self.q) == 1)
		except BaseException:
			self.assertTrue(True)

	def test_prep_object(self):
		a = self.c.control.prep_object()
		self.assertTrue((a[0] > 50 or a[0] < 1048576 * 2) and (a[1] == 1500 or a[1] == 576))

	def test_send_object(self): 
		self.c.control.send_object(True)
		self.c.control.send_object(False)

	def test_parse_object(self):
		x = TCP.time
		self.c.control.parse_object()
		self.assertTrue(x < TCP.time)

	def receive_packets(self): 
		self.c.control.receive_packets()
		self.assertTrue(True)

	def print_loss(self):
		self.c.control.process_loss()
		self.assertTrue(True)

	def test_get_tc(self):
		x = TCP.time
		tc = self.c.control.get_tc()
		self.assertTrue(tc > 0)
		self.assertTrue(x + tc == TCP.time)

	def test_add_to_q(self):
		x = len(self.c.control.q)
		self.c.control.add_to_q(Packet.TCP_SYN)
		self.assertTrue(len(self.c.control.q) - x == 2)

	def test_remove_from_q(self):
		self.c.control.add_to_q(Packet.TCP_SYN)
		x = len(self.c.control.q)
		self.c.control.remove_from_q()
		self.assertTrue(x - len(self.c.control.q) == 1)

if __name__ == '__main__':
	unittest.main()